package main;

import java.awt.Dimension;
import java.awt.Graphics;
import java.util.Random;

import imgs.Img;

interface MonsterState {
  void ping(Model m);
  void draw(Graphics g, Point center, Dimension size);
}

class Monster implements Entity{
  MonsterState activeMonster = null;
  int State = 0;
  private Point location;
  public Point location(){ return location; }
  public void location(Point p){ location=p; }
  Monster(Point location, boolean isRoaming, boolean isBoss){ 
    this.location=location; 
    if(isBoss){
      activeMonster = new Boss();
    }
    else if(activeMonster == null && isRoaming==false){
      System.out.println("Generating Normal Monster");
      activeMonster = new sleepingMonster();
    }
    else if(activeMonster == null && isRoaming==true){
      System.out.println("Generating Roaming Monster");
      activeMonster = new RoamingMonster();
    }
  }
  public MonsterState activeMonster(){
    return activeMonster;
  }
  public void setActiveMonsterRoaming(){
    activeMonster = new RoamingMonster();
  }
  public double speed(){ return 0.05d; }
  public Monster returnMonst(){
    return this;
  }

  public void ping(Model m){
      activeMonster.ping(m);
  }

  public void draw(Graphics g, Point center, Dimension size) {
      activeMonster.draw(g, center, size);
  }

  class AwakeMonster implements MonsterState{
    public void ping(Model m){
      if(location().distance(m.camera().location()).size() > 6 && (State!=0 || State!=2)){
        activeMonster = new sleepingMonster();
      }
      var arrow = m.camera().location().distance(location);
      double size = arrow.size();
      arrow = arrow.times(speed()/size);
      location = location.add(arrow); 
      if(size<0.6d){ m.onGameOver(); }
    }
   
    public double chaseTarget(Monster outer, Point target){
      var arrow = target.distance(outer.location());
      double size = arrow.size();
      arrow = arrow.times(speed()/size);
      outer.location(outer.location().add(arrow));
      return size;
    }
    public void draw(Graphics g, Point center, Dimension size) {
      drawImg(Img.AwakeMonster.image, g, center, size);
    }
  }

  class RoamingMonster implements MonsterState{
    Random random = new Random();
    Point target = new Point(random.nextInt(0,16),random.nextInt(1,17)+0);
    public int count = 0;
    public void ping(Model m){
      count++;
      if(count > 50){
        count = 0;
        target = new Point(random.nextInt(0,16),random.nextInt(1,17)+0);
      }
      chaseTarget(returnMonst(), target);
    }
    public double chaseTarget(Monster outer, Point target){
      var arrow = target.distance(outer.location());
      double size = arrow.size();
      arrow = arrow.times(speed()/size);
      outer.location(outer.location().add(arrow));
      return size;
    }
    public void draw(Graphics g, Point center, Dimension size) {
      drawImg(Img.RoamingMonster.image, g, center, size);
    }
  }
  
  class sleepingMonster implements MonsterState{
    public void ping(Model m){
      if(location().distance(m.camera().location()).size() < 6 && (State!=1 || State!=2)){
        activeMonster = new AwakeMonster();
      }
    }
    public void draw(Graphics g, Point center, Dimension size){
      drawImg(Img.SleepMonster.image,g,center,size);
    }
  }
  
  class deadMonster implements MonsterState{
    public int counter = 0;
    public void ping(Model m){
      counter++;
      if(counter > 100){
        m.remove(returnMonst());
      }
    }
    public void draw(Graphics g, Point center, Dimension size){
      drawImg(Img.DeadMonster.image,g,center,size);
    }
  }

  class Boss implements MonsterState{
    public void ping(Model m){
      var arrow = m.camera().location().distance(location);
      double size = arrow.size();
      arrow = arrow.times(speed()/size);
      location = location.add(arrow); 
      if(size<0.6d){ m.onGameOver(); }
    }
    public void draw(Graphics g, Point center, Dimension size){
      drawImg(Img.Boss.image,g,center,size);
    }
  }
}
