package main;

import java.util.List;

import main.Monster.deadMonster;

record Phase(Model model, Controller controller){ 
  
  static Phase level1(Runnable next, Runnable first) {
    Camera c = new Camera(new Point(5,5));
    Sword s = new Sword(c);
    Cells cells = new Cells();
    var m = new Model(){
      List<Entity> entities = List.of(c,s,new Monster(new Point(0,0), false,false));
      public Camera camera(){ return c; }
      public List<Entity> entities(){ return entities; }
      public void remove(Entity e){ 
        entities = entities.stream()
          .filter(ei->!ei.equals(e))
          .toList();
      }
      public Cells cells(){ return cells; }
      public void onGameOver(){ first.run(); }
      public void onNextLevel(){ next.run(); }
    };
    return new Phase(m,new Controller(c,s));    
  }

  static Phase level2(Runnable next, Runnable first) {
    Camera c = new Camera(new Point(5,5));
    Sword s = new Sword(c);
    Cells cells = new Cells();
    var m = new Model(){
      List<Entity> entities = List.of(c,s,new Monster(new Point(0,0), false,false), new Monster(new Point(16, 0), false,false),new Monster(new Point(0, 16), false,false), new Monster(new Point(16, 16), true,false));
      public Camera camera(){ return c; }
      public List<Entity> entities(){ return entities; }
      public void remove(Entity e){ 
        entities = entities.stream()
          .filter(ei->!ei.equals(e))
          .toList();
      }
      public Cells cells(){ return cells; }
      public void onGameOver(){ first.run(); }
      public void onNextLevel(){ next.run(); }
    };
    return new Phase(m,new Controller(c,s));    
  }
  static Phase level3(Runnable next, Runnable first) {
    Camera c = new Camera(new Point(5,5));
    Sword s = new Sword(c);
    Monster monst = new Monster(new Point(8,4), false,true);
    Sword bs = new Sword(monst){
      public Direction direction(){ return Direction.Left; }
      public double distance(){ return 1.5d; }
      public double speed(){ return 0.4d; }
      public void onHit(Model m, Entity e){
        if(e instanceof Camera){
          first.run();
        }
      }
      public void ping(Model m){
        if(userPerished()){
          m.remove(returnSword());
        }
        weaponRadiant+=direction().arrow(speed()).x();
        weaponRadiant%=Math.PI*2d;
        var l = this.location();
        m.entities().stream()
          .filter(e->e!=this)
          .filter(e->e.location().distance(l).size()<effectRange())
          .forEach(e->onHit(m,e));
      }
      public boolean userPerished(){
        if(wielder instanceof Monster){
          Monster monst = (Monster)wielder;
          if(monst.activeMonster instanceof deadMonster){
            return true;
          }
        }
        return false;
      }
    };

    Cells cells = new Cells();
    var m = new Model(){
      List<Entity> entities = List.of(c,s,monst,bs);
      public Camera camera(){ return c; }
      public List<Entity> entities(){ return entities; }
      public void remove(Entity e){ 
        entities = entities.stream()
          .filter(ei->!ei.equals(e))
          .toList();
      }
      public Cells cells(){ return cells; }
      public void onGameOver(){ first.run(); }
      public void onNextLevel(){ next.run(); }
    };
    return new Phase(m,new Controller(c,s));    
  }
}